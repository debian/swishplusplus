Description: fix FTBFS with GCC6
 replace ambigious references with sw_ prefixed ones.
Author: Tobias Frost <tobi@debian.org>
Bug-Debian: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=811797
Last-Update: 2016-09-23
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/index.c
+++ b/index.c
@@ -673,7 +673,7 @@
 //
 // SYNOPSIS
 //
-        inline int rank( int file_index, int occurences_in_file, double factor )
+        inline int sw_rank( int file_index, int occurences_in_file, double factor )
 //
 // DESCRIPTION
 //
@@ -883,7 +883,7 @@
                     continues = true;
                 o << enc_int( file->index_ )
                   << enc_int( file->occurrences_ )
-                  << enc_int( rank(file->index_, file->occurrences_, factor) );
+                  << enc_int( sw_rank(file->index_, file->occurrences_, factor) );
                 if ( !file->meta_ids_.empty() )
                     file->write_meta_ids( o );
 #ifdef  FEATURE_word_pos
@@ -930,7 +930,7 @@
                     continues = true;
                 o << enc_int( file->index_ )
                   << enc_int( file->occurrences_ )
-                  << enc_int( rank(file->index_, file->occurrences_, factor) );
+                  << enc_int( sw_rank(file->index_, file->occurrences_, factor) );
                 if ( !file->meta_ids_.empty() )
                     file->write_meta_ids( o );
 #ifdef  FEATURE_word_pos
@@ -995,7 +995,7 @@
         //
         double const factor = (double)Rank_Factor / info.occurrences_;
         TRANSFORM_EACH( word_info::file_list, info.files_, file )
-            file->rank_ = rank( file->index_, file->occurrences_, factor );
+            file->rank_ = sw_rank( file->index_, file->occurrences_, factor );
     }
 
     if ( verbosity > 1 )
--- a/stem_word.c
+++ b/stem_word.c
@@ -48,7 +48,7 @@
     bool        (*condition)( char const *word );
 };
 
-static char *end; // iterator at end of word being stemmed
+static char *sw_end; // iterator at end of word being stemmed
 // Acess to this global variable is protected by the cache_lock mutex in
 // stem_word().
 
@@ -112,10 +112,10 @@
 //
 //*****************************************************************************
 {
-    if ( end - word < 3 )
+    if ( sw_end - word < 3 )
         return false;
 
-    register char const *c = end;
+    register char const *c = sw_end;
     return !(is_vowel( *--c ) || *c == 'w' || *c == 'x' || *c == 'y' ) &&
             (is_vowel( *--c ) || *c == 'y') && !is_vowel( *--c );
 }
@@ -206,7 +206,7 @@
 #   endif
 
     for ( ; rule->id; ++rule ) {
-        register char *const suffix = end - rule->old_suffix_len;
+        register char *const suffix = sw_end - rule->old_suffix_len;
         if ( suffix < word )
             continue;
 
@@ -226,7 +226,7 @@
 #           ifdef DEBUG_stem_word
             cerr << "---> replaced word=" << word << "\n";
 #           endif
-            end = suffix + rule->new_suffix_len;
+            sw_end = suffix + rule->new_suffix_len;
             break;
         }
         *suffix = ch;                           // no match: put back
@@ -397,7 +397,7 @@
 
     char word_buf[ Word_Hard_Max_Size ];
     ::strcpy( word_buf, word );
-    end = word_buf + len;
+    sw_end = word_buf + len;
 
     replace_suffix( word_buf, rules_1a );
     int const rule = replace_suffix( word_buf, rules_1b );
